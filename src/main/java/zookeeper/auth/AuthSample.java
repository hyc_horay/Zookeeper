package main.java.zookeeper.auth;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;

/**
 * 简单的使用包含权限信息的Zookeeper会话创建数据节点
 * @author Tony
 *
 */
public class AuthSample implements Watcher {

	final static String PATH="/zk-book-auth-test";
	
	public static void main(String[] args) throws Exception {
		ZooKeeper zooKeeper=new ZooKeeper("127.0.0.1:2181", 5000,new AuthSample());
		zooKeeper.addAuthInfo(SchemeEnum.DIGEST.getCode(), "foo:true".getBytes());
		zooKeeper.create(PATH, "123".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		Thread.sleep(Integer.MAX_VALUE);
	}

	@Override
	public void process(WatchedEvent event) {
		System.out.println("receive watch event :"+event);
	}
}
