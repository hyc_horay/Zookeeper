package main.java.zookeeper.auth;

/**
 * Ȩ����Ϣ
 * @author Tony
 *
 */
public enum SchemeEnum {

	WORLD("world"),
	
	AUTH("auth"),
	
	IP("ip"),
	
	DIGEST("digest");
	
	SchemeEnum(String code){
		this.code=code;
	}
	
	private String code;

	public String getCode() {
		return code;
	}
	
	public static void main(String[] args) {
		System.out.println(SchemeEnum.DIGEST.getCode());
	}
}
