package main.java.zookeeper.auth;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;


/**
 * 对于删除而言，起作用是其子节点，当我们对一个数据节点添加权限后，依然可以自由的删除这个节点
 * 但是对于该节点的子节点就必须使用相应的权限才能删除它。
 * 
 * @author Tony
 *
 */
public class AuthSample_Delete_Test implements Watcher {

	
	final static String PATH  = "/zk-book-auth_test";
    final static String PATH2 = "/zk-book-auth_test/child";
    
    public static void main(String[] args) throws Exception {
    	AuthSample_Delete_Test authSample_Delete_Test=new AuthSample_Delete_Test();
    	
    	 ZooKeeper zookeeper1 = new ZooKeeper("127.0.0.1:2181",5000,authSample_Delete_Test);
         zookeeper1.addAuthInfo("digest", "foo:true".getBytes());
         zookeeper1.create( PATH, "init".getBytes(), Ids.CREATOR_ALL_ACL, CreateMode.PERSISTENT );
         zookeeper1.create( PATH2, "init".getBytes(), Ids.CREATOR_ALL_ACL, CreateMode.EPHEMERAL );
    	
         
         try {
 			ZooKeeper zookeeper2 = new ZooKeeper("127.0.0.1:2181",50000,authSample_Delete_Test);
 			zookeeper2.delete( PATH2, -1 );
 		} catch ( Exception e ) {
 			System.out.println( "删除节点失败: " + e.getMessage() );
 		}
         
         ZooKeeper zookeeper3 =new ZooKeeper("127.0.0.1:2181",5000,authSample_Delete_Test);
         zookeeper3.addAuthInfo("digest", "foo:true".getBytes());
 		zookeeper3.delete( PATH2, -1 );
         System.out.println( "成功删除节点：" + PATH2 );
         
         ZooKeeper zookeeper4 = new ZooKeeper("127.0.0.1:2181",5000,authSample_Delete_Test);
 		zookeeper4.delete( PATH, -1 );
         System.out.println( "成功删除节点：" + PATH );
         
         
    }
    
    @Override
	public void process(WatchedEvent event) {
	}
}
