package main.java.zookeeper;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;

/**
 * 基本的回话实例
 * Zookeeper客户端与服务端会话建立是一个异步的过程，
 * 并没有真正建立一个可用的会话，在会话周期中处于“CONNECTING”状态
 *
 */
public class ZooKeeper_Constructor_Usage_Simple implements Watcher {

	private static CountDownLatch countDownLatch=new CountDownLatch(1);
	
	public static void main(String[] args) {
		try {
			ZooKeeper zooKeeper=new ZooKeeper("127.0.0.1:2181", 500, new ZooKeeper_Constructor_Usage_Simple());
		    System.out.println(zooKeeper.getState());
		    countDownLatch.await();
		    System.out.println("Zookeeper session established.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	public void process(WatchedEvent event) {
		System.out.println("receive watch event :"+event);
		if(KeeperState.SyncConnected== event.getState()){
			countDownLatch.countDown();
		}
	}

}
